import React from "react";

import "../../App.css";

const Footer = () => {
  return (
    <div className="navbar navbar-dark bg-dark mt-5">
      <div className="navbar-brand mb-0 h2 mx-auto">
        Coded by{" "}
        <a
          href="https://eldrik.gitlab.io/"
          target="_blank"
          rel="noopener noreferrer"
          className="badge badge-light"
        >
          Karel-František Houf
        </a>
        {" | "}
        Lyrics delivered from{" "}
        <a
          href="https://www.musixmatch.com/"
          target="_blank"
          rel="noopener noreferrer"
          className="badge badge-light"
        >
          musixmatch.com
        </a>
      </div>
    </div>
  );
};

export default Footer;

import React, { Component } from "react";
import { Consumer } from "../../context";
import axios from "axios";

class Search extends Component {
  state = {
    track_title: ""
  };

  onChange = event => {
    // event.target.name --> name="track_title" in the input tag ==> REUSABLE
    this.setState({ [event.target.name]: event.target.value });
  };

  onFormSubmit = (dispatch, event) => {
    event.preventDefault();

    axios.get(`https://cors-anywhere.herokuapp.com/https://api.musixmatch.com/ws/1.1/track.search?q_track=${this.state.track_title}&page_size=10&page=1&s_track_rating=desc&apikey=${process.env.REACT_APP_MM_KEY}`)
      .then(res => {
        dispatch({
          type: 'SEARCH_TRACKS',
          payload: res.data.message.body.track_list
        });

        this.setState({ track_title: '' });
      })
      .catch(console.log)
  }

  render() {
    return (
      <Consumer>
        {value => {
          const { dispatch } = value;

          return (
            <div className="card card-body mb-4 p-4 shadow-lg bg-light">
              <h1 className="display-4 text-center">
                <i className="fas fa-music" /> Search for a song{" "}
                <i className="fas fa-music" />
              </h1>
              <p className="lead text-center">
                Get the lyrics for any song you want!
              </p>
              <form onSubmit={this.onFormSubmit.bind(this, dispatch)}>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Song title..."
                    name="track_title"
                    value={this.state.track_title}
                    onChange={this.onChange}
                  />
                </div>
                <button
                  type="submit"
                  className="btn btn-primary btn-lg btn-block mb-5"
                >
                  {" "}
                  Get Song Lyrics{" "}
                </button>
              </form>
            </div>
          );
        }}
      </Consumer>
    );
  }
}

export default Search;
